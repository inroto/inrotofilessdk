<?php
/**
 * Created by PhpStorm.
 * User: Toms
 * Date: 14.11.9
 * Time: 09:52
 */

class InrotoFilesFiles {

    private $_xmlData;
    private $_arrayData;
    private $_latestFileID;

    /* @var $Wrapper InrotoFiles */
    private $Wrapper;

    public function __construct($Wrapper) {
        $this->_latestFileID = -1;
        $this->Wrapper = $Wrapper;
    }

    public function getSomething() {
        $this->Wrapper->echoSomething();
    }

    public function fileDataArray($Filters = array(), $FileID = 0, $LoadData = true){
        $ret = array();
        if (($LoadData) OR ($this->_latestFileID != $FileID)) {
            $this->loadFileData($FileID,$Filters);
        }

        $json = json_encode($this->_xmlData);
        $ret = $this->addZeroElementToFileArray(json_decode($json,TRUE));
        $this->_arrayData = $ret;

        return $ret;
    }

    private function addZeroElementToFileArray(array $array){
        $ret = array();
        if(!isset($array['file'][0])){
            $ret['file'][] = $array['file'];
        }else{
            $ret = $array;
        }
        return $ret['file'];
    }

    public function loadFileData($FileID = 0, $Filters = array()){
        $URL = "";

        if($FileID > 0){
            $URL .= "";
        }else{
            $URL .= "xml/file/list";
        }

        $this->_latestFileID = $FileID;
        $this->_xmlData = $this->Wrapper->loadData($URL, $Filters);

        return $this->_xmlData;

    }
}
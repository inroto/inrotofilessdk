<?php
/**
 * Created by PhpStorm.
 * User: Māris Bahtins
 * Date: 9/29/2014
 * Time: 4:06 PM
 */

class InrotoFilesMetadata{
    private $_metadataXml = "";
    private $_metadataArray = array();
    private $_metadataValuesArray = array();

    private $_htmlOptionArray = array();

    /* @var $Wrapper InrotoFiles */
    private $Wrapper;

    public function __construct($Wrapper) {
        $this->Wrapper = $Wrapper;
    }
    /**
     * @return array()
     */
    public function getMetadataArray($Filters = array(), $LoadData = true) {
        return ($this->_loadMetadata($Filters, $LoadData)) ? $this->_metadataArray : null;
    }

    private function _loadMetadata($Filters = array(), $LoadData = true){
        if($LoadData) {
            $URL = "xml/metadata/item/metadata";
            $this->_metadataXml = $this->Wrapper->loadData($URL, $Filters);
            $this->_metadataArray = json_decode(json_encode($this->_metadataXml), TRUE);

            /* TODO: make global as function */
            if(isset($this->_metadataArray['values'])){
                if(isset($this->_metadataArray['values']['value'][0])){
                    $this->_metadataValuesArray = $this->_metadataArray['values']['value'];
                }else{
                    $this->_metadataValuesArray[] = $this->_metadataArray['values']['value'];
                }
            }else{
                $this->_metadataValuesArray[] = array('value_id'=>'', 'value_type'=>'','showvalue'=>'');
            }

            unset($this->_metadataArray['values']);
            $this->_metadataArray['values'] = $this->_metadataValuesArray;
        }
        return $LoadData;
    }

    /**
     * array('filterName','filterName2');
    */
    public function getMetadataComboboxArray($Filters = array()){
        $ret = array();
            if(array_count($Filters) > 0 && $this->_loadMetadata($Filters)){
                foreach($this->_metadataValuesArray as $value){
                    $ret[$value['showvalue']] = $this->_metadataArray['attr_name'] . ' ' . $value['showvalue'];
                    $this->_htmlOptionArray[$value['showvalue']] = array('data-filter-name'=>$this->_metadataArray['filter_name']);
                }

//                $ret = $this->_metadataValuesArray;
//                $ret = $this->_metadataArray;
            }

        return $ret;
    }


    /**
     * @return array
    */
    public function getHTMLOption(){
        return $this->_htmlOptionArray;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: Toms
 * Date: 14.2.9
 * Time: 11:13
 * Version: 1.0.0
 * GIT/Project:  https://bitbucket.org/tomskovkajevs/inrotofiles
 *
 * Version history:
 *
 * 1.0.0
 * date: 11.09.2014
 *
 * ...
 *
 *
 * ===========================================
 *
 * Path generation and CLASS usage
 *
 * Generate new Class object in development mode (You have specific data/XML server:
 *  $Ob = new InrotoFiles('http://local.server.com');
 *
 * To use public server (files.inroto.com), do not provide any URL:
 *  $Ob = new InrotoFiles();
 *
 * While developing XML's and this class, you need to consider using 2 variables and
 * one specific part of path to get DATA:
 *
 *  http://server.com/ api/xml/ product/list/?filterparam1=1&..
 *  <----- 1 --------> <- 2 --> <----------- 3 --------------->
 *
 * 1) Param that can be provided when create nev Class instance ($Ob = new InrotoFiles('this param / URL');
 * 2) This path is built in this CLASS and will change only in case VERSIONS change (not in first versions)
 * 3) This is specific to method and dataset needed. This is not changeable and is specified by the
 *    developer who creates data processing nethod and/or developer creating datasets.
 *
 *
*/
class InrotoFiles {
    /* @var $Products InrotoFilesProducts */
    /* @var $Files InrotoFilesFiles */
    /* @var $Metadata InrotoFilesMetadata */

    private $_url;
    private $_innerSubUrl;

    private $dummy1;

    public $Products;
    public $Files;
    public $Metadata;
    public $Subscriptions;

    public function __construct($URL = '') {
        $this->_latestProductID = NULL;
        $this->_innerSubUrl = 'api/';

        $this->Products = new InrotoFilesProducts($this);
        $this->Files = new InrotoFilesFiles($this);
        $this->Metadata = new InrotoFilesMetadata($this);
        $this->Subscriptions = new InrotoFilesSubscriptions($this);


        if (strlen($URL) == 0) {
            $URL = 'http://files.inroto.lv/';
        }
        $this->_url = $URL;
    }

    public function _getDataCURL($url) {
       // echo "<br>".$url."<br>";
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        libxml_use_internal_errors(true);


        $doc = new DOMDocument();
        if (@$doc->loadXML($data) === false) {
            die("Something is wrong 2x (InrotoFiles-get_data..) {".$url."} {{".$data."}}");
        }

        $source1 = $doc->saveXML();
        $source = simplexml_load_string($source1);
        if ($source === false) {
            echo "Failed loading XML\n";
            foreach(libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
        }
        return $source;
    }
    function get_array_main_key($needle, $haystack){
        $ret = null;
        foreach($haystack as $key=>$value){
            if(is_array($value)){
                foreach($value as $subK=>$subV){
                    if($subK == $needle){
                        $ret = $key;
                    }
                }
            }
        }
        return $ret;
    }

    public function loadData($Path,$Filters=array()) {
        $URL = $this->getUrl();
        $FilterString = "";
        $AndVal = "";


        foreach ($Filters as $KeyX=>$Val) {
            if(is_array($Val)){
                foreach($Val as $KeySub=>$ValSub){
                    if(is_array($ValSub)){
                        foreach($ValSub as $kS=>$vS){
                            if(is_array($vS)){
                                foreach($vS as $kSsub=>$vSsub){
                                    $FilterString .= $AndVal.urlencode($KeySub).'['.urlencode($kS).']['.urlencode($kSsub).']='.urlencode($vSsub);
                                    $AndVal = "&";
                                }
                            }else{
                                $FilterString .= $AndVal.urlencode($KeySub).'['.urlencode($kS).']='.urlencode($vS);
                                $AndVal = "&";
                            }
                        }
                    }
                }
            }else{
                $FilterString .= $AndVal.urlencode($KeyX)."=".urlencode($Val);
                $AndVal = "&";
            }
        }
        $And = "";
        if (strlen($FilterString) > 0) {
            $And = "?";
        }
        $CallURL = $URL.$Path.$And.$FilterString;
        // echo $CallURL;
        return $this->_getDataCURL($CallURL);
    }

    /**
     *
     * @return string full url to data (like http://server.com/api/ )
     *
     * */
    public function getUrl($version = '1') {
        $ret = $this->_url.$this->_innerSubUrl;
        return $ret;
    }

    /*
    * Check array_key_existe multi dim array.
    * Param and return same as array_key_exists
    */
    public function array_key_exists_r($needle, $haystack)
    {
        $result = array_key_exists($needle, $haystack);
        if ($result) return $result;
        foreach ($haystack as $v) {
            if (is_array($v)) {
                $result = $this->array_key_exists_r($needle, $v);
            }
            if ($result) return $result;
        }
        return $result;
    }


    /**
     * @param array $data
     * @return Exeption 404
     * */
    public function errorPageNotExists($data){
        if(isset($data[0])){
            throw new Exception('The requested page does not exist. (produced by InrotoFiles SDK)', 404);
        }
        return $data;
    }

    public function echoSomething($dam = '') {

        if (strlen($dam) > 0) {
            $this->dummy1 = $dam;
        }
        echo "This is InrotoFiles class [".$this->dummy1."] ...";
    }


    /**
     * Funkcija izveido serverim pieprasīju, ar dotiem parametriem.
     * Ja ir viss ir veiksmīgi, tad funkcijas atgriezīs url, kur iespējams lejupielādēt
     * pieprasītos failus.
     * @param $id int product id.
     * @param $rev string file rev string.
     * @return $data encode json format.
    */
    public function getDownloadZip($id, $rev, $type, $appID){
        $zipUrl = "";

        $zipUrl = $this->getUrl() . "default/zip?{$type}ids[{$id}]={$rev}&appid={$appID}";

        //echo json_encode($this->_getDataCURL($zipUrl));
        return json_encode($this->_getDataCURL($zipUrl));
    }

    /**
     * $
    */
    public function getProductFilterArray($filterArray = array(),$UserEmail = null,$AppID = ''){
        $testFunk = false;
        $ret = array();

            if(count($filterArray) == 0){
                $ret = $this->Products->getProductDataArray($filterArray,0,true,$UserEmail,$AppID);
            }else{
                $urlArray = array();
                foreach($filterArray as $value){
                    list($filterType,$filterName, $filterValue) = explode("_", $value);

                    if($this->array_key_exists_r($filterType,$urlArray)){
                        if($this->array_key_exists_r($filterName, $urlArray)){
                            if(is_array($urlArray[$this->get_array_main_key($filterType, $urlArray)][$filterType][$filterName])){
                                $urlArray[$this->get_array_main_key($filterType, $urlArray)][$filterType][$filterName][] = $filterValue;
                            }else{
                                $urlArray[$this->get_array_main_key($filterType, $urlArray)][$filterType][$filterName] = array($urlArray[$this->get_array_main_key($filterType, $urlArray)][$filterType][$filterName], $filterValue);
                            }
                        }else{
                            $urlArray[$this->get_array_main_key($filterType, $urlArray)][$filterType][$filterName] = $filterValue;
                        }
                    }else{
                        $urlArray[][$filterType] = array($filterName=>$filterValue);
                    }
                }
                if($testFunk){
                    echo "<pre>";
                    print_r($this->Products->getProductDataArray($urlArray,0,true,$UserEmail,$AppID));
                }
                $ret = $this->Products->getProductDataArray($urlArray, 0, true, $UserEmail,$AppID);
            }

        return $ret;
    }

    public function _handleZeroElement($comeInto = array(), $type = ""){
        $ret = array();
        if(isset($comeInto[$type][0])) {
            $ret = $comeInto[$type];
        }else{
            if(isset($comeInto[$type])){
                $ret[] = $comeInto[$type];
            }else{
                $ret = $comeInto;
            }

        }
        return $ret;
    }
}

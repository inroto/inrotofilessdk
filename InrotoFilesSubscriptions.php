<?php
/**
 * Created by PhpStorm.
 * User: Toms
 * Date: 03.01.2015.
 * Time: 17:42
 */


// Subscriptions


class InrotoFilesSubscriptions {

    private $_xmlData;
    private $_arrayData = array();

    private $subscriptionTypes = array(
        'product'=>1,
        'file'=>2,
        'package'=>3
    );

    /* @var $Wrapper InrotoFiles */
    private $Wrapper;

    public function __construct($Wrapper) {
        $this->Wrapper = $Wrapper;
    }


    /**
     *
     *
     * @param $Type int 0=File, 1=Product, 2=Package
     * @param $ObjectID int
     * @param $Email string
     * @return boolean
     */
    public function addSubscription($Type, $ObjectID, $Email,$AppID) {
        $ret = false;

        $URL = "";
        if(($Type >= 0) && ($ObjectID > 0) && (strlen($Email) > 0)){
            $URL .= "xml/subscriptions/add/?type=".(int)$Type."&ob=".(int)$ObjectID."&email=".$Email."&appid=".$AppID;

            $this->_xmlData = $this->Wrapper->loadData($URL);

            $json = json_encode($this->_xmlData);
            $decode = json_decode($json,TRUE);
            if(isset($decode[0])){
                echo "Nekas netika atrasts";
                die();
            }

            //$ret .= "[".$URL."][[[".$this->_xmlData."]]]";//print_r($decode,true);
            $ret = $decode;

        }


        return $ret;
    }

    /**
     *
     *
     * @param $Type int 0=File, 1=Product, 2=Package
     * @param $ObjectID int
     * @param $Email string
     * @return boolean
     */
    public function toggleSubscription($Type, $ObjectID, $Email,$AppID) {
        $ret = false;

        $URL = "";
        if(($Type >= 0) && ($ObjectID > 0) && (strlen($Email) > 0)){
            $URL .= "xml/subscriptions/toggle/?type=".(int)$Type."&ob=".(int)$ObjectID."&email=".$Email."&appid=".$AppID;

            $this->_xmlData = $this->Wrapper->loadData($URL);

            $json = json_encode($this->_xmlData);
            $decode = json_decode($json,TRUE);
            if(isset($decode[0])){
                echo "Nekas netika atrasts";
                die();
            }

            //$ret .= "[".$URL."][[[".$this->_xmlData."]]]";//print_r($decode,true);
            $ret = $decode;

        }


        return $ret;
    }

    /**
     * @return boolean
    */
    public function addProduct($ProdID, $Email,$AppID) {

        $ret = $this->addSubscription($this->subscriptionTypes['product'],$ProdID,$Email,$AppID);

        return $ret;

    }

    /**
     * @return boolean
     */
    public function toggleProduct($ProdID, $Email,$AppID) {

        $ret = $this->toggleSubscription($this->subscriptionTypes['product'],$ProdID,$Email,$AppID);

        return $ret;

    }

}
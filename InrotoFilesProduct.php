<?php
/**
 * Created by PhpStorm.
 * User: Toms
 * Date: 14.11.9
 * Time: 09:52
 */



class InrotoFilesProducts{

    private $_xmlData;
    private $_arrayData = array();
    private $_latestProductID;
    private $_arrayFileInProductData = array();

    /* @var $Wrapper InrotoFiles */
    private $Wrapper;

    public function __construct($Wrapper) {
        $this->_latestProductID = -1;
        $this->Wrapper = $Wrapper;
    }

    public function getSomething() {
        $this->Wrapper->echoSomething();
    }


    /**
     * @return array
     */
    public function getProductDataArray($Filters = array(),$ProductID = 0, $LoadData = true, $UserEmail = null,$AppID = '') {
        $ret = array();
        if (($LoadData) OR ($this->_latestProductID != $ProductID)) {
            $this->loadProductData($ProductID,$Filters,$UserEmail,$AppID);
        }
//          echo $this->_xmlData."<hr>";
        $json = json_encode($this->_xmlData);
        $decode = json_decode($json,TRUE);
            if(isset($decode[0])){
                echo "Nekas netika atrasts";
                die();
            }

        $ret = $this->_handleComeIntoProductArray($decode);
//        $ret = $decode;
        $this->_arrayData = $ret;

        return $ret;
    }

    /**
     * @param $decodedArray array
     * @return $ret array
    */
    private function _handleComeIntoProductArray($decodedArray){
        $ret = array();
            $decodedArray = $this->Wrapper->_handleZeroElement($decodedArray, 'product');
        /* Šis nostrādās kad ir zināms produkta id */
        if(isset($decodedArray['product_id'])){
           // print_r($decodedArray);
            $ret = array('file_thumb'=> "");
                foreach($decodedArray as $dKey => $dValue){
                    if(isset($dKey) && $dKey == 'product_public_metadata'){
                        foreach($dValue as $subMetaKey => $subMetaValue){
                            $ret[$subMetaKey] = $subMetaValue;
                        }
                    }else if(isset($dKey)&& $dKey == 'product_public_thumbnail'){
                        if(isset($dValue['file_thumb_'. (array_count($dValue)-1)])){
                            $ret['file_thumb'] = $dValue['file_thumb_'. (array_count($dValue)-1)];
                        }

                    }else{
                        $ret[$dKey] = $dValue;
                    }
                }
            if(isset($decodedArray['files'])){
                $this->_handleFileInProduct($decodedArray);
                unset($ret['files']);
            }
        }else{
            foreach($decodedArray as $dKey => $dValue){
                $tempDecodedArray = array('file_thumb'=> "");
                    foreach($dValue as $subDkey => $subDValue){

                        if(isset($subDkey) && $subDkey == 'product_public_metadata'){
                            foreach($subDValue as $subMetaKey => $subMetaValue){
                                $tempDecodedArray[$subMetaKey] = $subMetaValue;
                            }
                        }else if(isset($subDkey)&& $subDkey == 'product_public_thumbnail'){
                            $ThumbCnt = array_count($subDValue);
                            $ThumbId = $ThumbCnt-1;
                            $tempDecodedArray['file_thumb'] = "";
                            if (isset($subDValue['file_thumb_'. $ThumbId])) {
                                $tempDecodedArray['file_thumb'] = $subDValue['file_thumb_'. $ThumbId];
                            }

                        }else{
                            $tempDecodedArray[$subDkey] = $subDValue;
                        }
                    }
                $ret[] = $tempDecodedArray;
            }
        }
        return $ret;
    }

    private function _handleFileInProduct($filesArray){
        if(isset($filesArray['files']['file'][0])){
            $this->_arrayFileInProductData = $filesArray['files']['file'];
        }else{
            $this->_arrayFileInProductData[] = $filesArray['files']['file'];
        }
    }

    /**
     * @return array
    */
    public function fileProductDataArray($AppID = ''){
        // TODO: Add AppID check? need this?
        return $this->_arrayFileInProductData;
    }

//    private function _handleZeroElement($comeInto = array(), $type = ""){
//        $ret = array();
//            if(isset($comeInto[$type][0])) {
//                $ret = $comeInto[$type];
//            }else{
//                if(isset($comeInto[$type])){
//                    $ret[] = $comeInto[$type];
//                }else{
//                    $ret = $comeInto;
//                }
//
//            }
//        return $ret;
//    }


    public function productDummySlider() {
        $ret = array();
        $Filters = array();

        $this->_latestProductID = 0;
        $this->_xmlData = $this->Wrapper->loadData("xml/product/driverslider",$Filters);

        $json = json_encode($this->_xmlData);
        $ret = json_decode($json,TRUE);
        $this->_arrayData = $ret;

        return $ret;
    }



    /**
     * This function is processing simple product data and stores it to inner XML or RETURNS
     *
     * This class should not be called directly and there should be method to check if CACHE has
     * up-to-date data to be used.
     *
     * @return string xml document contents
     */
    public function loadProductData($ProductID = 0,$Filters = array(),$UserEmail = '',$AppID = '') {

        $URL = "";
        $Email = "";

        if (strlen($UserEmail) > 0) {
            $Email = "/email/".urlencode($UserEmail);
        }
        if($ProductID > 0){
            $URL .= "xml/product/item/id/".(int)$ProductID.$Email."/appid/".$AppID;
        } else {
            $URL .= "xml/product/list".$Email."/appid/".$AppID;
        }
        $this->_latestProductID = $ProductID;
        $this->_xmlData = $this->Wrapper->loadData($URL,$Filters);
        return $this->_xmlData;
    }

}